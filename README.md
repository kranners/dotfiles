# dotfiles

MacOS dotfiles for my personal developing environment.

## 🏗️ Installation
To install, first clone the environment into a seperate folder:
```shell
git clone --bare git@gitlab.com:kranners/dotfiles.git $HOME/.dotfiles
```

Then checkout the rest of the repository:
```shell
git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME checkout
```
