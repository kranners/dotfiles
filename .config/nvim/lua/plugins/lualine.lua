-- lualine.nvim configuration

require('lualine').setup({
  options = {
    theme = 'onedark',
  },
})
