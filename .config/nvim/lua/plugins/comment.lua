-- Commenter

local comment = require('Comment')

comment.setup({
  -- I like to get bounced around by the comments, like in VSCode
  sticky = false,
})

