-- whichkey.nvim configuration

-- remove space normal mode mapping
vim.api.nvim_set_keymap('n', '<Space>', '', {})

-- remap space to leaderkey
vim.g.mapleader = ' '

local wk = require('which-key')
local comment = require('Comment.api')
local esc = vim.api.nvim_replace_termcodes('<ESC>', true, false, true)


wk.setup {}

wk.register(
  {
    ['<LEADER>'] = {
      ["<LEADER>"] = { "<CMD>WhichKey<CR>", "Show bindings" },
      t = { '<cmd>NvimTreeFocus<cr>', 'Toggle file tree' },
      T = { '<cmd>NvimTreeClose<cr>', 'Collapse file tree' },
      f = {
        name = 'Find',
        f = { '<cmd>Telescope find_files<cr>', 'Find file' },
        s = { '<cmd>Telescope live_grep<cr>', 'Find string' },
        b = { '<cmd>Telescope buffers<cr>', 'Find buffer' },
        r = { '<cmd>Telescope oldfiles<cr>', 'Find recent file' },
      },
      u = {
        name = 'Packer',
        c = { '<cmd>PackerCompile<cr>', 'Compile' },
        s = { '<cmd>PackerSync<cr>', 'Sync' },
        S = { '<cmd>PackerStatus<cr>', 'Status' },
        u = { '<cmd>PackerUpdate<cr>', 'Update' },
      },
      p = {
        name = 'Pane',
        v = { '<CMD>vsplit<CR>', 'Create vertical split' },
        s = { '<CMD>split<CR>', 'Create horizontal split' },
      },
      s = {
        name = "Source",
        v = { '<cmd>source $MYVIMRC<cr>', 'Source in vimrc' },
      },
    },
    ['<C-h>'] = { '<cmd>NavigatorLeft<cr>', 'Move left' },
    ['<C-j>'] = { '<cmd>NavigatorDown<cr>', 'Move down' },
    ['<C-k>'] = { '<cmd>NavigatorUp<cr>', 'Move up' },
    ['<C-l>'] = { '<cmd>NavigatorRight<cr>', 'Move right' },
    ['<M-Left>'] = { '<cmd>vertical resize +5<cr>', 'Resize pane left' },
    ['<M-Down>'] = { '<cmd>horizontal resize +5<cr>', 'Resize pane down' },
    ['<M-Up>'] = { '<cmd>horizontal resize -5<cr>', 'Resize pane up' },
    ['<M-Right>'] = { '<cmd>vertical resize -5<cr>', 'Resize pane right' },
    ['<C-_>'] = { comment.toggle.linewise.current, 'Toggle line comment' },
  },
  {
    mode = 'n'
  }
)

-- Visual mode mappings
wk.register(
  {
    ['<C-_>'] = { function()
      vim.api.nvim_feedkeys(esc, 'nx', false)
      comment.toggle.blockwise(vim.fn.visualmode())
    end, 'Toggle block comment' },
  },
  {
    mode = 'v'
  }
)

-- Insert mode mappings
-- With iTerm2 you can enable Command+/ support for this!
-- Navigate to Preferences -> Profiles -> Keys -> Key Mappings
-- and then add a command for "Send Hex Codes: 0x1f", mapped to Command+/
wk.register(
  {
    ['<C-_>'] = { comment.toggle.linewise.current, 'Toggle line comment' },
  },
  {
    mode = 'i'
  }
)
