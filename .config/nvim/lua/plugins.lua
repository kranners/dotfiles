-- plugins.lua

local ensure_packer = function()
  local fn = vim.fn
  local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
  if fn.empty(fn.glob(install_path)) > 0 then
    fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
    vim.cmd [[packadd packer.im]]
    return true
  end
  return false
end

local packer_bootstrap = ensure_packer()

return require('packer').startup(function(use)
  -- Package manager
  use 'wbthomason/packer.nvim'

  -- Configurations for Nvim LSP
  use 'neovim/nvim-lspconfig'

  -- Enable devicons for anything that can use them
  use 'nvim-tree/nvim-web-devicons'

  -- Colorscheme
  use {
     'navarasu/onedark.nvim',
     config = function()
       require('plugins.onedark')
     end
  }

  -- Keybindings
  use {
    'folke/which-key.nvim',
    config = function()
      require('plugins.whichkey')
    end
  }

  -- File tree
  use {
    'nvim-tree/nvim-tree.lua',
    requires = {
      'nvim-tree/nvim-web-devicons', -- for file icons
    },
    config = function()
      require('plugins.nvimtree')
    end
  }

  -- tpope surrounding lua replacement
  use {
    'kylechui/nvim-surround',
    tag = '*',
    config = function()
      require('plugins.surround')
    end
  }

  -- Fancy statusbar
  use {
    'nvim-lualine/lualine.nvim',
    config = function()
      require('plugins.lualine')
    end
  }

  -- Telescope, fuzzy-finding, file searching, etc
  use {
    'nvim-telescope/telescope.nvim',
    tag = '0.1.0',
    requires = 'nvim-lua/plenary.nvim',
    config = function()
      require('plugins.telescope')
    end
  }

  -- Fancier startup screen, for loading into sessions
  use {
    'goolord/alpha-nvim',
    config = function()
      require('plugins.alpha')
    end
  }

  -- Session management
  use {
    'Shatur/neovim-session-manager',
    requires = { 'nvim-lua/plenary.nvim' },
    config = function()
      require('plugins.sessions')
    end
  }

  -- Navigator for tmux panes
  use {
    'numToStr/Navigator.nvim',
    config = function()
      require('plugins.navigator')
    end
  }

  -- coq autocomplete
  use {
    'ms-jpq/coq_nvim',
    branch = 'coq',
    config = function()
      require('plugins.coq')
    end
  }
  use {
	  'ms-jpq/coq.artifacts',
	  branch = 'artifacts'
  }

  use 'sheerun/vim-polyglot'

  -- Commenting in insert mode
  use {
    'numToStr/Comment.nvim',
    config = function()
      require('Comment').setup()
    end
  }

  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins
  if packer_bootstrap then
    require('packer').sync()
  end
end)
