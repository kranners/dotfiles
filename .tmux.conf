# Make colors work right.
set -g default-terminal "screen-256color"

# For certain commands, we don't want them to run in Neovim, since they're shared hotkeys.
# This is an evil string. Don't touch it. Look away.
is_nvim="ps -o state= -o comm= -t '#{pane_tty}' \
    | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?g?(view|n?vim?x?)(diff)?$'"

# More same prefix, 'C-b' to 'C-a'
unbind C-b
set-option -g prefix C-a
bind-key C-a send-prefix

# Binding ctrl-hjkl to pane movement
# If we're already in Neovim, this should be handled by Navigator
bind -n C-h if-shell "$is_nvim" 'send-keys C-h' 'select-pane -L'
bind -n C-j if-shell "$is_nvim" 'send-keys C-j' 'select-pane -D'
bind -n C-k if-shell "$is_nvim" 'send-keys C-k' 'select-pane -U'
bind -n C-l if-shell "$is_nvim" 'send-keys C-l' 'select-pane -R'

# Meta-arrow keys to resizing
# Note that if you're using MacOS, then this won't work by default.
# Use iTerm2, and under Preferences -> Profiles -> Keys, change Left Option Key to Esc+
# Same stuff here, for parity between Neovim and Tmux.
bind-key -n M-Left  if-shell "$is_nvim" 'send-keys M-Left' 'resize-pane -L'
bind-key -n M-Down  if-shell "$is_nvim" 'send-keys M-Down' 'resize-pane -D'
bind-key -n M-Up    if-shell "$is_nvim" 'send-keys M-Up' 'resize-pane -U'
bind-key -n M-Right if-shell "$is_nvim" 'send-keys M-Right' 'resize-pane -R'

# Adding in Chrome-like window creation/deletion
bind -n M-t new-window
bind -n M-w kill-window 

# To future me: the perfect solution here is Ctrl+tab, and Ctrl+shift+tab, just like Chrome.
# But that's not possible, because the suits at Big Terminal don't allow it.
# So instead I compromise with a much worse Meta-hjkl
bind-key -n M-l next-window
bind-key -n M-h previous-window

# Chrome-like window specific navigation
bind-key -n M-0 select-window -t :0
bind-key -n M-1 select-window -t :1
bind-key -n M-2 select-window -t :2
bind-key -n M-3 select-window -t :3
bind-key -n M-4 select-window -t :4
bind-key -n M-5 select-window -t :5
bind-key -n M-6 select-window -t :6
bind-key -n M-7 select-window -t :7
bind-key -n M-8 select-window -t :8

# Easier renaming of windows
bind -n M-r command-prompt -I "rename-window '%'"

# Ensure that new panes open in the current pane's path
bind % split-window -h -c '#{pane_current_path}'
bind '"' split-window -v -c '#{pane_current_path}'

# Plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'dracula/tmux' # Colorscheme

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run '~/.tmux/plugins/tpm/tpm'

